// @flow
/* eslint eqeqeq: "off" */

import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route } from 'react-router-dom';
import { Card, NavBar, ListGroup } from './widgets';
import ReactDOM from 'react-dom';

class Student {
  firstName: string;
  lastName: string;
  email: string;

  constructor(firstName: string, lastName: string, email: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }
}
let students = [
  new Student('Ola', 'Jensen', 'ola.jensen@ntnu.no'),
  new Student('Kari', 'Larsen', 'kari.larsen@ntnu.no')
];

class Course {
  code: string;
  title: string;

  constructor(code: string, title: string) {
    this.code = code;
    this.title = title;
  }
}
let courses = [
  new Course('TDAT3019', 'Systemutvikling 3'),
  new Course('TDAT3020', 'Sikkerhet i programvare og nettverk'),
  new Course('TDAT3022', 'Systemutviklingsprosjekt'),
  new Course('TDAT3023', '3D-grafikk med prosjekt'),
  new Course('TDAT3024', 'Matematikk og fysikk valgfag'),
  new Course('TDAT3025', 'Anvendt maskinlæring med prosjekt')
];

class CourseRegistration {
  courseCode: string;
  studentEmail: string;

  constructor(courseCode: string, studentEmail: string) {
    this.courseCode = courseCode;
    this.studentEmail = studentEmail;
  }
}
let courseRegistrations = [
  new CourseRegistration('TDAT3019', 'ola.jensen@ntnu.no'),
  new CourseRegistration('TDAT3025', 'ola.jensen@ntnu.no'),
  new CourseRegistration('TDAT3025', 'kari.larsen@ntnu.no')
];

class Menu extends Component {
  render() {
    return (
      <NavBar>
        <NavBar.Brand>React example</NavBar.Brand>
        <NavBar.Link to="/students">Students</NavBar.Link>
        <NavBar.Link to="/courses">Courses</NavBar.Link>
      </NavBar>
    );
  }
}

class Home extends Component {
  render() {
    return <Card title="React example with static pages">User input and application state are covered next week.</Card>;
  }
}

class StudentList extends Component {
  render() {
    return (
      <Card title="Students">
        <ListGroup>
          {students.map(student => (
            <ListGroup.Item key={student.email} to={'/students/' + student.email}>
              {student.firstName} {student.lastName}
            </ListGroup.Item>
          ))}
        </ListGroup>
      </Card>
    );
  }
}

class StudentDetails extends Component<{ match: { params: { email: string } } }> {
  render() {
    let student = students.find(student => student.email == this.props.match.params.email);
    if (!student) {
      console.error('Student not found');
      return null;
    }
    return (
      <Card title="Details">
        <ListGroup>
          <ListGroup.Item>First name: {student.firstName}</ListGroup.Item>
          <ListGroup.Item>Last name: {student.lastName}</ListGroup.Item>
          <ListGroup.Item>Email: {student.email}</ListGroup.Item>
        </ListGroup>
        <Card title="Courses">
          <ListGroup>
            {courseRegistrations
              .filter(registration => registration.studentEmail == student.email)
              .map(registration => {
                let course = courses.find(course => course.code == registration.courseCode);
                if (!course) return null;
                return <ListGroup.Item key={course.code}>{course.title}</ListGroup.Item>;
              })}
          </ListGroup>
        </Card>
      </Card>
    );
  }
}

class CourseList extends Component {
  render() {
    return (
      <Card title="Courses">
        <ListGroup>
          {courses.map(course => (
            <ListGroup.Item key={course.code} to={'/courses/' + course.code}>
              {course.title}
            </ListGroup.Item>
          ))}
        </ListGroup>
      </Card>
    );
  }
}

class CourseDetails extends Component<{ match: { params: { code: string } } }> {
  render() {
    let course = courses.find(course => course.code == this.props.match.params.code);
    if (!course) {
      console.error('Course not found');
      return null;
    }
    return (
      <Card title="Details">
        <ListGroup>
          <ListGroup.Item>Code: {course.code}</ListGroup.Item>
          <ListGroup.Item>Title: {course.title}</ListGroup.Item>
        </ListGroup>
        <Card title="Students">
          <ListGroup>
            {courseRegistrations.filter(registration => registration.courseCode == course.code).map(registration => {
              let student = students.find(student => student.email == registration.studentEmail);
              if (!student) return null;
              return (
                <ListGroup.Item key={student.email}>
                  {student.firstName} {student.lastName}
                </ListGroup.Item>
              );
            })}
          </ListGroup>
        </Card>
      </Card>
    );
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Menu />
        <Route exact path="/" component={Home} />
        <Route path="/students" component={StudentList} />
        <Route path="/students/:email" component={StudentDetails} />
        <Route path="/courses" component={CourseList} />
        <Route path="/courses/:code" component={CourseDetails} />
      </div>
    </HashRouter>,
    root
  );
